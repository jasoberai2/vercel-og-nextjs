import { ImageResponse } from '@vercel/og'
import { NextRequest } from 'next/server'

export const config = {
  runtime: 'edge',
}

export default async function handler(req: NextRequest) {
  const { searchParams } = req.nextUrl
  const text = searchParams.get('text')
  if (!text) {
    return new ImageResponse(<>{'Visit with "?text=waiting"'}</>, {
      width: 1200,
      height: 630,
    })
  }

  return new ImageResponse(
    (
      <div
        id="post-teaser"
        className="sc-1wljadh-0 cLveDj"
        
        style={{
          width: "600px",
          backgroundColor: "#EDFAE9",
          display: 'flex',
          backgroundSize: "cover",
          boxSizing: "border-box",
        }}
      >
        <div
          className="sc-1wljadh-5 jpoGLe"
          style={{
            inset: "0px",
            padding: "4%",
            position: "absolute",
            backgroundColor: "#EDFAE9",

            display: "flex",
            flexDirection: "column",
            WebkitBoxPack: "justify",
            justifyContent: "space-between",
            boxSizing: "border-box",
          }}
        >
          <div
            className="sc-1wljadh-6 cOfnIN"
            style={{
              display: "flex",
              WebkitBoxPack: "justify",
              justifyContent: "space-between",
            }}
          >
            <span
              className="sc-caiLqq jgZmVR"
              style={{ display: "flex", height: "48px", width: "48px" }}
            >
              <svg
                viewBox="0 0 436 476"
                xmlns="http://www.w3.org/2000/svg"
                style={{
                  overflow: "hidden",
                  height: "48px",
                  width: "48px",
                  filter: "blur(4px)",
                  alignSelf: "center",
                  fill: "transparent",
                  strokeWidth: "1.2px",
                }}
              >
                
                <path
                  d="M436 143c-.084-60.778-47.57-110.591-103.285-128.565C263.528-7.884 172.279-4.649 106.214 26.424 26.142 64.089.988 146.596.051 228.883c-.77 67.653 6.004 245.841 106.83 247.11 74.917.948 86.072-95.279 120.737-141.623 24.662-32.972 56.417-42.285 95.507-51.929C390.309 265.865 436.097 213.011 436 143Z"
                  style={{
                    transition: "all 300ms cubic-bezier(0.19, 1, 0.22, 1) 0s",
                    vectorEffect: "non-scaling-stroke",
                    fill: "#07DA07",
                  }}
                />
              </svg>
            </span>
          </div>
          <span
            className="sc-dJjYzT hsKmhb"
            style={{
              marginLeft: "400px",
              paddingTop: "300px",
              fontFamily:
                'ABC Oracle Plus Variable,-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Arial,sans-serif',
              fontWeight: 500,
              lineHeight: 1.3,
              letterSpacing: "-0.02em",
              color: "#07DA07",
              fontSize: "26px",
            }}
          >
            <div
              className="sc-2qvsbh-0 fWdjFF"
              style={{ fontWeight: 325, fontSize: "32px", 
              marginTop: "-120px"}}
            >
              {text} 
            </div>
          </span>
        </div>
      </div>
    ),
    {
      width: 2400,
      height: 1260,
    }
  )
}
